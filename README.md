# install postgresql  
```  
kubectl create namespace database  
helm repo add azure-marketplace https://marketplace.azurecr.io/helm/v1/repo  
helm repo update  
helm install my-release azure-marketplace/postgresql  
kubectl get secret --namespace database my-postgresql -o jsonpath="{.data.postgres-password}"    
```  
## connect to the server and run psql commands  
```  
kubectl exec -it my-postgresql --namespace database -- /opt/bitnami/scripts/postgresql/entrypoint.sh /bin/bash  
```  
